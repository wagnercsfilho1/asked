angular.module("hoby")

    .filter('hashtag', function () {
        return function (obj) {
            if (obj)
                return obj.replace(/#(\S*)/g, '<a ng-click="searchHash(\'$1\')" href>#$1</a>');
            else
                return obj;
        }
    })

    .filter('searchUserLike', function() {
        return function(input, id) {
            if (input) {
                for (var i = 0; i < input.length; i++) {
                    if (input[i] == id) {
                        return true;
                    }
                }
            }
            return false;
        }
    });

