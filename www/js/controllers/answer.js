angular.module("hoby")

    .controller('AnswerCtrl', function ($scope, $rootScope, PostService, CommentService, NotificationService) {
        var id = state.getCurrentPage().options.id;

        $scope.text = null;
        $scope.post = null;


        PostService.getById(id).then(function (data) {
            $scope.post = data;
        });

        $scope.sendComment = function () {
            CommentService.save($scope.text, $scope.post, $scope.currentUser).then(function (comment) {
                PostService.respond($scope.post).then(function(newpost){
                    PostService.setBestComment(comment, newpost).then(function (data) {
                        PostService.pushComments(newpost, data).then(function(){
                            NotificationService.create({from_user: $scope.currentUser, user: $scope.post.get('user'), type: 'comment'}).then(function () {
                                PostService.delete($scope.post).then(function(){
                                    $rootScope.$emit('notification.comments', $scope.post.get('user').id);
                                    $rootScope.$emit('myQuestions.respond', $scope.post);
                                    $rootScope.backView();
                                });
                            });
                        });
                    });
                })

                $scope.text = null;
            });
        }

    })