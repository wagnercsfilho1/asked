angular.module("hoby")

    .controller('ComposeCtrl', function ($scope, $sce, $rootScope, $timeout, video, $cordovaGeolocation, $cordovaCapture, $cordovaCamera, PostService, CategoryService) {

        $scope.toUser = state.getCurrentPage().options.user || null;

        $scope.text = null;
        $scope.image = null;
        $scope.anonymous = false;
        $scope.votePic = false;
        $scope.selectedCategory = null;
        $scope.optionPic = {
            one: '',
            two: ''
        }

        CategoryService.getAll().then(function(results){
          $scope.categories = results;
        });

        $scope.setVotePic = function(){
            $scope.votePic = true;
        }

        var createPost = function (image, type, votePic1, votePic2) {
            var hashtags = $scope.text.match(/#.+?\b/g);

            PostService.save({
                text: $scope.text,
                user: $scope.currentUser,
                type: type,
                category: $scope.selectedCategory,
                anonymous: $scope.anonymous,
                media: image,
                hashtags: hashtags,
                touser: $scope.toUser,
                vote_picture_one: votePic1,
                vote_picture_two: votePic2,
                approved: $scope.toUser != null ? false : true
            }).then(function (data) {

                if ($scope.toUser) {
                    $rootScope.$emit('notification.question', $scope.toUser.id);
                } else {
                    $rootScope.$emit('newUserPost', data);
                    $rootScope.$emit('notification.timeline');
                }

                $scope.load = false;
                $scope.backView();
            }, function (err) {
                console.log('error save');
                $scope.load = false;
            });
        }


        $scope.getCamera = function (sourceType) {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: sourceType || 1,
                mediaType: 0,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.image = imageData;
            }, function (err) {
                // error
            });
        }

        $scope.getVotePic = function (picOpt, sourceType) {

            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: sourceType || 1,
                mediaType: 0,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.optionPic[picOpt] = imageData;
            }, function (err) {
                // error
            });
        }

        $scope.send = function () {
            $scope.load = true;
            if ($scope.image) {
                var imageName = new Date().getTime();
                var parseFile = new Parse.File(imageName, { base64: $scope.image });
                parseFile.save().then(function () {
                    createPost(parseFile, 'image');
                });
            } else if ($scope.optionPic.one && $scope.optionPic.two){
                var parseFile1 = new Parse.File(new Date().getTime(), { base64: $scope.optionPic.one });
                parseFile1.save().then(function () {
                    var parseFile2 = new Parse.File(new Date().getTime(), { base64: $scope.optionPic.two });
                    parseFile2.save().then(function () {
                        createPost(null, 'vote', parseFile1, parseFile2);
                    });
                });
            } else {
                createPost(null, 'status');
            }
        }

    })
