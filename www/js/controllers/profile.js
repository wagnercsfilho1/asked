angular.module("hoby")

    .controller('ProfileCtrl', function ($scope, PostService, FollowService, UserService) {
        var id = state.getCurrentPage().options.id;

        $scope.posts = null;
        $scope.isFollow = false;

        UserService.getById(id).then(function (data) {
            $scope.profile = data;

            FollowService.isFollow($scope.profile).then(function (data) {
                $scope.isFollow = data.length === 0 ? false : true;
            });

            PostService.getByUser($scope.profile).then(function (data) {
                $scope.posts = data;
            });
        });


        $scope.follow = function () {

            FollowService.followUser($scope.profile).then(function (data) {
                $scope.isFollow = true;
            }, function (err) {
                console.log(err);
            });
        }

        $scope.unFollow = function () {

            FollowService.unFollowUser($scope.profile).then(function (data) {
                $scope.isFollow = false;
            }, function (err) {
                console.log(err);
            });
        }


    })