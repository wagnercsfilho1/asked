angular.module("hoby")

    .controller("MainCtrl", function ($scope, $rootScope, $window, $firebaseObject, amMoment, $translate, AuthService, FollowService) {

        $rootScope.$watch('currentUser', function () {
            console.log(AuthService.currentUser().id)
            var ref = new Firebase("https://asked.firebaseio.com/notifications/" + AuthService.currentUser().id);
            $rootScope.realNotifications = $firebaseObject(ref);
        })

        $scope.changeLanguage = function (langKey) {
            $window.localStorage.language = langKey;
            amMoment.changeLocale(langKey);
            $translate.use(langKey);
        }

        $rootScope.$on('notification.question', function (event, data) {
            var ref = new Firebase("https://asked.firebaseio.com/notifications/" + data);
            $scope.realQuestions = $firebaseObject(ref);
            $scope.realQuestions.$loaded().then(function () {
                $scope.realQuestions.questions = $scope.realQuestions.questions == null ? 1 : $scope.realQuestions.questions + 1;
                $scope.realQuestions.$save();
            });
        });

        $rootScope.$on('notification.comments', function (event, data) {
            var ref = new Firebase("https://asked.firebaseio.com/notifications/" + data);
            $scope.realComment = $firebaseObject(ref);
            $scope.realComment.$loaded().then(function () {
                $scope.realComment.comments = $scope.realComment.comments == null ? 1 : $scope.realComment.comments + 1;
                $scope.realComment.$save();
            });
        });

        $rootScope.$on('notification.timeline', function (event, data) {
            FollowService.getFollowingIds($scope.currentUser).then(function (data) {
                angular.forEach(data, function (data) {
                    var ref = new Firebase("https://asked.firebaseio.com/notifications/" + data.get('following').id);
                    $scope.realTimeline = $firebaseObject(ref);
                    $scope.realTimeline.$loaded().then(function () {
                        $scope.realTimeline.timeline = $scope.realTimeline.timeline == null ? 1 : $scope.realTimeline.timeline + 1;
                        $scope.realTimeline.$save();
                    });
                });
            })

        });
    })