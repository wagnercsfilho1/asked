angular.module("hoby")

    .controller('HomeCtrl', function ($scope, $rootScope, $firebaseObject, $timeout, PostService, FollowService) {
        var usersIds = [];
        var page = 0;
        var noLoadMore = false;

        $scope.posts = null;
        $scope.done = true;
        $scope.usersIds = [];
        $scope.tabIndex = 0;
        $scope.elHideSubNavBar = true;

        var ref = new Firebase("https://asked.firebaseio.com/notifications/" + $rootScope.currentUser.id);
        $scope.notifications = $firebaseObject(ref);
        $scope.notifications.$loaded().then(function () {
            $scope.notifications.timeline = 0;
            $scope.notifications.$save();
        });

        $scope.hideSubNavBar = function(){
            $timeout(function(){
                $scope.elHideSubNavBar = false;
            }, 0)
        }

        $scope.showSubNavBar = function(){
            $timeout(function(){
                $scope.elHideSubNavBar = true;
            }, 0)
        }

        $scope.friends = function () {
            $scope.tabIndex = 0;
            page = 0;
            $scope.posts = null;
            noLoadMore = false;
            $scope.usersIds = [];
            $scope.usersIds.push($scope.currentUser);

            FollowService.getFollowingIds($scope.currentUser).then(function (data) {
                angular.forEach(data, function (data) {
                    $scope.usersIds.push(data.get('following'));
                });
                PostService.getFromFriends($scope.usersIds, page).then(function (data) {
                    $scope.posts = data;
                    page += 1;
                });
            });
        }

        $scope.recents = function () {
            $scope.tabIndex = 1;
            page = 0;
            $scope.posts = null;
            noLoadMore = false;

            PostService.getRecents($scope.usersIds,page).then(function (data) {
                $scope.posts = data;
                page += 1;
            });
        }

        $scope.load = function(){
            $scope.done = false;
            page = 0;
            noLoadMore = false;
            var getFunc = $scope.tabIndex == 0 ? 'getFromFriends' : 'getRecents';
            PostService[getFunc]($scope.usersIds, page).then(function (data) {
                if (data.length > 0) {
                    $scope.posts = data;
                }
                $scope.done = true;
            });
        }

        $rootScope.$on('newUserPost', function (event, data) {
            $scope.posts.unshift(data);
        });

        $timeout(function(){
            var page_content = document.getElementsByClassName("page--home__content");
            page_content[0].addEventListener("scroll", function(){
                if( (page_content[0].scrollTop + page_content[0].clientHeight) >= page_content[0].scrollHeight ) {
                    if (!noLoadMore) {
                        $scope.loadMore = true;
                        var getFunc = $scope.tabIndex == 0 ? 'getFromFriends' : 'getRecents';
                        PostService[getFunc]($scope.usersIds, page).then(function (data) {
                            if (data.length == 0) {
                                noLoadMore = true;
                                $scope.loadMore = false;
                            } else {
                                angular.forEach(data, function (value) {
                                    $scope.posts.push(value);
                                });
                                page += 1;
                                $scope.loadMore = false;
                            }
                        });
                    }
                }
            });

        },0);

        $scope.friends();

    })
    