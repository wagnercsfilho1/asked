angular.module("hoby")

    .controller('QuestionCtrl', function ($scope, $rootScope, $filter, PostService, CommentService, NotificationService) {
        var id = state.getCurrentPage().options.id;
        $scope.comments = null;
        $scope.text = null;
        $scope.post = null;
        $scope.anonymous = false;


        PostService.getById(id).then(function (data) {
            $scope.post = data;

            CommentService.getByPost(data).then(function (data) {
                $scope.comments = data;
            });
        });

        $scope.setBestComment = function (comment) {

            ons.notification.confirm({
                message: $filter('translate')('ARE_YOU_SURE'),
                callback: function(idx) {
                    switch(idx) {
                        case 0:
                            break;
                        case 1:
                            PostService.setBestComment(comment, $scope.post).then(function (data) {
                            });
                            break;
                    }
                }
            });

        }

        $scope.sendComment = function () {
          //  CommentService.save($scope.text, $scope.post, $scope.currentUser, $scope.anonymous).then(function (data) {

                PostService.pushComments($scope.post, {
                    text: $scope.text,
                    anonymous: $scope.anonymous,
                    user:{
                        id: $rootScope.currentUser.id,
                        facebook_id: $rootScope.currentUser.get('facebook_id'),
                        avatar: 'https://graph.facebook.com/' + $rootScope.currentUser.get('facebook_id') + '/picture',
                        full_name: $rootScope.currentUser.get('full_name')
                    }
                }).then(function(){
                    NotificationService.create({from_user: $scope.currentUser, user: $scope.post.get('user'), type: 'comment', question: $scope.post}).then(function () {
                        $rootScope.$emit('notification.comments', $scope.post.get('user').id);
                    });

                    $scope.comments.push(data);
                    $scope.text = null;
                });


            //});
        }

        $scope.deleteComment = function(comment, idx){
            ons.notification.confirm({
                message: $filter('translate')('ARE_YOU_SURE'),
                callback: function(idx) {
                    switch(idx) {
                        case 0:
                            break;
                        case 1:
                            CommentService.delete(comment).then(function(data){
                                PostService.deleteComment($scope.post, comment);
                                console.log(idx);
                                $scope.comments.splice(idx, 1);
                            });
                            break;
                    }
                }
            });

        }


        $scope.likeComment = function (comment) {
            console.log('ss')
            CommentService.like(comment).then(function () {
            });
        }

        $scope.unLikeComment = function (comment) {
            CommentService.unLike(comment).then(function () {
            });
        }


    })