angular.module("hoby")

    .controller('LoginCtrl', function ($scope, $rootScope,$q, $cordovaGeolocation, AuthService, UserService, FollowService, OpenFBService) {


        $scope.facebookLogin = function () {
            OpenFBService.login({scope: 'public_profile, friends'}).then(function (response) {
                $rootScope.token = response.authResponse.token;

                OpenFBService.getInfo().then(function (data) {

                    var authData = {
                        id: String(data.id),
                        access_token: response.authResponse.token,
                        expiration_date: new Date(
                            new Date().getTime()
                        ).toISOString()
                    }

                    Parse.FacebookUtils.logIn(authData, {
                        success: function (user) {

                            user.set("facebook_id", data.id);
                            user.set("first_name", data.first_name);
                            user.set("last_name", data.last_name);
                            user.set("full_name", data.name);
                            user.set("email", data.email);
                            user.set("locale", data.locale);
                            user.set('gender', data.gender);
                            user.set("avatarSmall", 'https://graph.facebook.com/' + data.id + '/picture');
                            user.set("avatarMedium", 'https://graph.facebook.com/' + data.id + '/picture?type=large');
                            user.save(null, {
                                success: function (user) {
                                    $rootScope.currentUser = user;

                                    OpenFBService.getFriends().then(function (friends) {

                                        var friendIds = [];
                                        angular.forEach(friends.data, function (value) {
                                            friendIds.push(value.id);
                                        });

                                        UserService.findByFacebookIds(friendIds).then(function (users) {

                                            async.each(users, function(user, callback) {

                                                FollowService.isFollow(user).then(function(is){
                                                    if (is.length == 0){
                                                        FollowService.followUser($rootScope.currentUser, user).then(function(){
                                                            callback();
                                                        }, function(){
                                                            callback();
                                                        });
                                                    } else {
                                                        callback();
                                                    }
                                                }, function(){
                                                    callback();
                                                });
                                            }, function(err){
                                                // if any of the file processing produced an error, err would equal that error
                                                if( err ) {
                                                    console.log('Failed to process');
                                                } else {
                                                    $scope.view('tabs');
                                                }
                                            })
                                        });
                                    }, function () {
                                        $scope.view('tabs');
                                    });
                                },
                                error: function () {
                                    alert("Error Login. Try again.");
                                }
                            })

                        },
                        error: function (user, error) {
                            alert("User cancelled the Facebook login or did not fully authorize.");
                        }
                    })

                });
            }, function (err) {
                alert(err.error);
            });
        }
    })