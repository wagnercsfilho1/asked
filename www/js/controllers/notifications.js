angular.module("hoby")

    .controller('NotificationsCtrl', function ($scope, $rootScope, $firebaseObject, NotificationService) {
        $scope.notifications = [];

        var ref = new Firebase("https://asked.firebaseio.com/notifications/" + $rootScope.currentUser.id);
        $scope.updateNotification = $firebaseObject(ref);
        $scope.updateNotification.$loaded().then(function () {
            $scope.updateNotification.comments = 0;
            $scope.updateNotification.$save();
        });

        NotificationService.getByUser($rootScope.currentUser).then(function (data) {
            $scope.notifications = data;
        });
    })