angular.module("hoby")

    .controller('QuestionsCtrl', function ($scope,  $firebaseObject, $rootScope, PostService) {
        $scope.posts = [];

        var ref = new Firebase("https://asked.firebaseio.com/notifications/" + $rootScope.currentUser.id);
        $scope.updateQuestions = $firebaseObject(ref);
        $scope.updateQuestions.$loaded().then(function () {
            $scope.updateQuestions.question = 0;
            $scope.updateQuestions.$save();
        });

        PostService.getQuestions($scope.currentUser).then(function (data) {
            $scope.posts = data;
        });

    });