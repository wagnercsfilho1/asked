angular.module("hoby")

    .controller('TimelineCtrl', function ($scope, $rootScope, $filter, $timeout, PostService, CommentService, NotificationService) {
        $scope.likePost = function (post) {
            PostService.like(post).then(function () {
                NotificationService.create({from_user: $rootScope.currentUser, user: post.get('user'), type: 'likes', question: post}).then(function () {
                    $rootScope.$emit('notification.comments', post.get('user').id);
                });
            });
        }

        $scope.unLikePost = function (post) {
            PostService.unLike(post).then(function () {

            });
        }

        $scope.deletePost = function(post, idx){
            ons.notification.confirm({
                message: $filter('translate')('ARE_YOU_SURE'),
                callback: function(idx) {
                    switch(idx) {
                        case 0:
                            break;
                        case 1:
                            PostService.delete(post).then(function () {
                                $scope.posts.splice(idx-1, 1);
                            });
                            break;
                    }
                }
            });

        }
    });
