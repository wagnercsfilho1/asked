angular.module("hoby")

    .controller('AccountCtrl', function ($scope, $rootScope, $timeout, AuthService) {

        $scope.info = {
            first_name: $rootScope.currentUser.get('first_name'),
            last_name: $rootScope.currentUser.get('last_name'),
            username: $rootScope.currentUser.get('username'),
            bio: $rootScope.currentUser.get('bio')
        }

        $scope.saveInfo = function () {
            $rootScope.currentUser.set("first_name", $scope.info.first_name);
            $rootScope.currentUser.set("last_name", $scope.info.last_name);
            $rootScope.currentUser.set("full_name", $scope.info.first_name + ' ' + $scope.info.last_name);
            $rootScope.currentUser.set("username", $scope.info.username);
            $rootScope.currentUser.set("bio", $scope.info.bio);

            $rootScope.currentUser.save(null, {
                success: function (data) {

                }
            });
            modalInfo.hide();
        }

        $scope.changeAvatar = function () {
            var options = {
                quality: 70,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 50,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.image = imageData;
            }, function (err) {
                // error
            });
        }

        $scope.logOut = function () {
            AuthService.logOut();
            $rootScope.currentUser = null;
            $scope.resetView('login');
        }

    })