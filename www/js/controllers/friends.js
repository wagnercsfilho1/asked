angular.module("hoby")

    .controller('FriendsCtrl', function ($scope, $rootScope, FollowService) {
        $scope.peoples = [];

        FollowService.getFollowing($rootScope.currentUser).then(function (data) {
            $scope.peoples = data;
        });
    })