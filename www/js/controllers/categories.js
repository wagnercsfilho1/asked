angular.module("hoby")

    .controller("CategoriesCtrl", function ($scope, CategoryService) {
        $scope.categories = [];

        CategoryService.getAll().then(function(results){
          $scope.categories = results;
        });

    })
