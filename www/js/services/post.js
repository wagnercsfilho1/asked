angular.module("hoby")

    .factory('PostService', function ($q) {

        var Post = Parse.Object.extend("Post");

        return {
            all: function () {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);

                query.include("user");

                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            find: function (q) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);

                query.include("user");
                query.startsWith('text', q);
                query.equalTo('approved', true);

                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getById: function (id) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.include("user");
                query.include("touser");
                query.include('best');
                query.include('best.user');
                query.get(id, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            save: function (data) {
                var deferred = $q.defer();

                var post = new Post();
                post.save(data, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            setBestComment: function (comment, post) {
                var deferred = $q.defer();
                post.set('best', comment);
                post.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            respond: function (question) {
                var deferred = $q.defer();
                question.set('approved', true);
                var post = new Post();
                post.save(question, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            delete: function(post){
                var deferred = $q.defer();

                post.destroy({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getFromFriends: function (users, page) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.containedIn("user", users);
                query.equalTo('approved', true);
                query.include("user");
                query.include("touser");
                query.include('best');
                query.include('best.user');
                query.include('category');
                query.descending("createdAt");
                query.limit(10);
                query.skip((page || 0) * 10);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getRecents: function (users, page) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.equalTo('approved', true);
                query.include("user");
                query.include("touser");
                query.include('best');
                query.include('best.user');
                query.descending("createdAt");
                query.limit(10);
                query.skip((page || 0) * 10);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            findByHashtags: function (q, page) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.containsAll("hashtags", [q]);
                query.include("user");
                query.include("touser");
                query.include('best');
                query.include('best.user');
                query.equalTo('approved', true);
                query.skip((page || 0) * 10);
                query.descending("createdAt");
                query.limit(15);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(object);
                    }
                });

                return deferred.promise;
            },

            getByUser: function (user, page) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.equalTo("user", user);
                query.equalTo("anonymous", false);
                query.equalTo('approved', true);
                query.include("user");
                query.include("touser");
                query.include('best');
                query.include('best.user');
                query.skip((page || 0) * 10);
                query.descending("createdAt");
                query.limit(15);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getQuestions: function (user, page) {
                var deferred = $q.defer();

                var query = new Parse.Query(Post);
                query.equalTo("touser", user);
                query.equalTo('approved', false);
                query.include("user");
                query.skip((page || 0) * 10);
                query.descending("createdAt");
                query.limit(15);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            like: function (post) {
                var deferred = $q.defer();

                var user = Parse.User.current();
                post.add("likes", user.id);
                post.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            unLike: function (post) {
                var deferred = $q.defer();

                var user = Parse.User.current();
                post.remove("likes", user.id);
                post.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            pushComments: function (post, comment) {
                var deferred = $q.defer();

                post.add("comments", comment);
                post.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            deleteComment: function (post, comment) {
                var deferred = $q.defer();

                post.remove("comments", comment);
                post.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            }
        }
    })
