angular.module("hoby")

    .factory('UserService', function ($q) {
        return {

            create: function (data) {
                var deferred = $q.defer();

                var user = new Parse.User();
                user.set("username", data.username);
                user.set("password", data.password);
                user.set("email", data.email);
                user.set("first_name", data.firstName);
                user.set("last_name", data.lastName);
                user.set("full_name", data.firstName + ' ' + data.lastName);

                user.signUp(null, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getById: function (id) {
                var deferred = $q.defer();

                var query = new Parse.Query(Parse.User);
                query.get(id, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            findByFacebookIds: function (ids) {
                var deferred = $q.defer();

                var query = new Parse.Query(Parse.User);
                query.containedIn("facebook_id", ids);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getNearby: function (position) {
                var deferred = $q.defer();
                var point = new Parse.GeoPoint({latitude: position.latitude, longitude: position.longitude});
                var query = new Parse.Query(Parse.User);
                query.near("location", point);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            }

        }
    })