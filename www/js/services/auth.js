angular.module("hoby")

    .service('AuthService', function ($q, $timeout) {
        return {
            currentUser: function () {
                return Parse.User.current();
            },

            logIn: function (user) {
                var deferred = $q.defer();

                Parse.User.logIn(user.username, user.password, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            logOut: function () {
                $timeout(function () {
                    Parse.User.logOut();
                }, 0);
            }
        }
    })