angular.module("hoby")

    .factory('OpenFBService', function ($q) {

        return {
            login: function () {
                var deferred = $q.defer();
                openFB.login(
                    function (response) {
                        if (response.status === 'connected') {
                            //alert('Facebook login succeeded, got access token: ' + response.authResponse.token);
                            deferred.resolve(response);
                        } else {
                            deferred.reject(response)
                        }
                    }, {scope: 'email, user_friends'});

                return deferred.promise;
            },
            getInfo: function () {
                var deferred = $q.defer();
                openFB.api({
                    path: '/me',
                    success: function (data) {
                        deferred.resolve(data);
                        //document.getElementById("userPic").src = 'http://graph.facebook.com/' + data.id + '/picture?type=small';
                    },
                    error: function (err) {
                        deferred.reject(err);
                    }});
                return deferred.promise;
            },
            getFriends: function () {
                var deferred = $q.defer();
                openFB.api({
                    path: '/me/friends',
                    success: function (data) {
                        deferred.resolve(data);
                        //document.getElementById("userPic").src = 'http://graph.facebook.com/' + data.id + '/picture?type=small';
                    },
                    error: function (err) {
                        deferred.reject(err);
                    }});
                return deferred.promise;
            }
        }
    })