angular.module("hoby")

    .factory('FollowService', function ($q) {

        var Follow = Parse.Object.extend("Follow");

        return {
            followUser: function (fromUser, toUser) {
                var deferred = $q.defer();

                var follow = new Follow();
                follow.set('follower', fromUser);
                follow.set('following', toUser);
                follow.save(null, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            unFollowUser: function (user) {
                var deferred = $q.defer();

                var currentUser = Parse.User.current();

                var query = new Parse.Query(Follow);
                query.equalTo("follower", currentUser);
                query.equalTo("following", user);
                query.first({
                    success: function (follow) {
                        follow.destroy({
                            success: function (data) {
                                deferred.resolve(data);
                            },
                            error: function (object, error) {
                                deferred.reject(error);
                            }
                        })
                    },
                    error: function (obj, err) {
                        deferred.reject(error);
                    }

                });

                return deferred.promise;
            },

            isFollow: function (user) {
                var deferred = $q.defer();

                var currentUser = Parse.User.current();
                var query = new Parse.Query(Follow);
                query.equalTo("follower", currentUser);
                query.equalTo("following", user);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (obj, err) {
                        deferred.reject(error);
                    }

                });

                return deferred.promise;
            },

            getFollowers: function (user) {
                var deferred = $q.defer();

                var query = new Parse.Query(Follow);
                query.include("follower");
                query.equalTo("following", user);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (obj, err) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getFollowing: function (user) {
                var deferred = $q.defer();

                var query = new Parse.Query(Follow);
                query.include("following");
                query.equalTo("follower", user);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (obj, err) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getFollowingIds: function (user) {
                var deferred = $q.defer();

                var query = new Parse.Query(Follow);
                query.equalTo("follower", user);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (obj, err) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            }

        }
    })