angular.module("hoby")

    .service('CommentService', function ($q) {

        var Comment = Parse.Object.extend("Comment");

        return {

            save: function (text, post, user, anonymous ) {
                var deferred = $q.defer();

                var comment = new Comment();
                comment.set('text', text);
                comment.set('post', post);
                comment.set('user', user);
                comment.set('anonymous', anonymous || false);
                comment.save(null, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            delete: function (comment) {
                var deferred = $q.defer();;

                comment.destroy({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            getByPost: function (post) {
                var deferred = $q.defer();

                var query = new Parse.Query(Comment);
                query.equalTo("post", post);
                query.include("user");
                //query.descending("createdAt");
                query.limit(15);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            like: function(comment){
                var deferred = $q.defer();

                var user = Parse.User.current();
                comment.add("likes", user.id);
                comment.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            unLike: function(comment){
                var deferred = $q.defer();

                var user = Parse.User.current();
                comment.remove("likes", user.id);
                comment.save({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;

            }
        }
    })