angular.module("hoby")

    .service('CategoryService', function ($q) {

        var Category = Parse.Object.extend("Category");

        return {

            getAll: function () {
                var deferred = $q.defer();

                var query = new Parse.Query(Category);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            }
          }
        });
