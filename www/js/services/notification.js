angular.module("hoby")

    .factory('NotificationService', function ($q) {

        var Notification = Parse.Object.extend("Notification");

        return {
            getByUser: function (user) {
                var deferred = $q.defer();

                var query = new Parse.Query(Notification);
                query.equalTo("user", user);
                query.include("from_user");
                query.include("question");
                query.include("question.user");
                query.descending("createdAt");
                query.limit(15);
                query.find({
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },

            create: function (data) {
                var deferred = $q.defer();

                var notification = new Notification();
                notification.save(data, {
                    success: function (data) {
                        deferred.resolve(data);
                    },
                    error: function (object, error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },
        }

    })
