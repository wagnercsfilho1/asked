angular.module("hoby", ['onsen', 'ngSanitize', 'pascalprecht.translate', 'ngVideo', 'firebase', 'ngCordova', 'angularMoment'])

    .config(function ($translateProvider) {
        $translateProvider.translations('en', {
            TAB_FRIENDS: 'Friends',
            TAB_RECENTS: 'New',
            TAB_POPULAR: 'Popular',

            INTRO_TITLE: 'Ask and Answer your friends',
            INTRO_DESCRIPTION: 'Ask and answer - the whole world , about anything - anytime !',

            ANSWERED_YOUR_QUESTION: 'Answed your question',
            LIKE_YOUR_QUESTION: 'Likes your question',
            SIGN_IN_FACEBOOK: 'Sign In with Facebook',

            ANSWERS: 'Answers',
            ANONYMOUS: 'Anonymous',
            BEST_ANSWER: 'Best answer',
            ANSWER: 'Answer',
            COMMENT: 'Comment',
            LIKE: 'Like',
            ASKED_TO: 'asked',
            FRIENDS: 'Friends',
            CAMERA: 'Camera',
            PHOTO_ALBUM: 'Photo Album',

            SEARCH: 'Search',

            ASK_SOMETHING: 'Ask Something',
            ASK_ANONYMOUSLY: 'Ask anonymously',
            ASK_QUESTIONS: 'Ask Questions',
            ANSWER_ANONYMOUS: 'Answer anonymous',

            PROFILE: 'Profile',
            ARE_YOU_SURE: 'Are you sure?',

            ACCOUNT: 'Account',
            MY_PROFILE: 'My Profile',
            EDIT_ACCOUNT: 'Edit Account',
            EDIT_PROFILE_INFO: 'Profile Info',
            LANGUAGE: 'Language',
            PORTUGUESE_BRAZIL: 'Portuguese - Brazil',
            ENGLISH: 'English',

            SIGN_OUT: 'Sign Out',


            RELIGION: 'Religion',
            TRAVEL: 'Travel',
            SPORTS: 'Sports',
            SCIENCE: 'Science',
            PREGNANCY: 'Pregnancy',
            PETS: 'Pets',
            POLITICS: 'Politic',
            NEWS: 'News',
            HEALTH: 'Health',
            GAMES: 'Games',
            FOOD: 'Foods',
            FAMILY: 'Family',
            ENTERTAINMENT: 'Entertainment',
            EDUCATION: 'Education',
            TECNOLOGY: 'Tecnology',
            CARS: 'Cars',
            BUSINESS: 'Business',
            BEAUTY: 'Beauty',
            FOTOGRAFY: 'Fotografy'
        });
        $translateProvider.translations('pt-br', {
            TAB_FRIENDS: 'Amigos',
            TAB_RECENTS: 'Recentes',
            TAB_POPULAR: 'Popular',


            INTRO_TITLE: 'Pergunte e Responda seus amigos',
            INTRO_DESCRIPTION: 'Pergunte e responda - à todo mundo, sobre qualquer coisa - a qualquer hora!',
            SIGN_IN_FACEBOOK: 'Entre com Facebook',

            ANSWERED_YOUR_QUESTION: 'Respondeu sua pergunta',
            LIKES_YOUR_QUESTION: 'Curtiu sua pergunta',

            PROFILE: 'Perfil',
            ANSWERS: 'Respostas',
            ANONYMOUS: 'Anônimo',
            BEST_ANSWER: 'Melhor Resposta',
            ANSWER: 'Responder',
            COMMENT: 'Comentar',
            LIKE: 'Curtir',
            ASKED_TO: 'perguntou para',
            FRIENDS: 'Amigos',
            CAMERA: 'Câmera',
            PHOTO_ALBUM: 'Álbum de Fotos',


            SEARCH: 'Pesquisar',

            ASK_SOMETHING: 'Pergunte algo',
            ASK_ANONYMOUSLY: 'Perguntar anonimamente',
            ASK_QUESTIONS: 'Pergunte',
            ANSWER_ANONYMOUS: 'Responder anonimamente',

            ARE_YOU_SURE: 'Você tem certeza?',

            ACCOUNT: 'Conta',
            MY_PROFILE: 'Meu Perfil',
            EDIT_ACCOUNT: 'Editar Conta',
            EDIT_PROFILE_INFO: 'Perfil',
            LANGUAGE: 'Idioma',
            PORTUGUESE_BRAZIL: 'Português - Brasil',
            ENGLISH: 'Inglês',

            SIGN_OUT: 'Sair',

            RELIGION: 'Religião',
            TRAVEL: 'Viagem',
            SPORTS: 'Esportes',
            SCIENCE: 'Ciência',
            PREGNANCY: 'Gravidez',
            PETS: 'Animais de Estimação',
            POLITICS: 'Política',
            NEWS: 'Notícias e Eventos',
            HEALTH: 'Saúde',
            GAMES: 'Jogos',
            FOOD: 'Comida',
            FAMILY: 'Família & Relacionamento',
            ENTERTAINMENT: 'Entretenimento',
            EDUCATION: 'Educação',
            TECNOLOGY: 'Tecnologia',
            CARS: 'Carros',
            BUSINESS: 'Negócios',
            BEAUTY: 'Beleza',
            FOTOGRAFY: 'Fotografia'

        });
        $translateProvider.preferredLanguage('en');
    })

    .run(function($rootScope, $translate, $window, $cordovaGeolocation, $cordovaGlobalization, AuthService, amMoment){

        if (!$window.localStorage.language){
            $cordovaGlobalization.getPreferredLanguage().then(
                function(result) {
                    console.log(result)
                },
                function(error) {
                    // error
                });
            $window.localStorage.language = 'en';
        }

        $translate.use($window.localStorage.language);
        amMoment.changeLocale($window.localStorage.language);

        openFB.init({appId: '772860432795258', tokenStore: window.localStorage});
        Parse.initialize("trnXIR3wp2dj2VKoEEJcBxQaIsPevOfPFnBkjH2c", "Zc5QKkYpcucxYXaOZfLahHpXNxgHfVC9HBojopPF");

        $rootScope.view = function(template, params){
            state.pushPage('views/'+template+'.html', angular.extend(params || {}, { animation: "none" }));
        }

        $rootScope.resetView = function(template, params){
            state.resetToPage('views/'+template+'.html', params);
        }

        $rootScope.backView = function(options){
            state.popPage(options);
        }

        $rootScope.searchHash = function(hashtag){
            $rootScope.view('search', {q: '#'+hashtag})
        }


        ons.ready(function() {
            if (AuthService.currentUser()){
                $rootScope.currentUser = AuthService.currentUser();
                console.log($rootScope.currentUser)
                $rootScope.resetView('tabs');
            } else {
                $rootScope.resetView('login');
            }

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(false);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });

    })
