angular.module("hoby")

    .directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(
                function (scope) {
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            )
        };
    }])

    .directive('whenScrolled', function () {
        return function (scope, elm, attr) {
            var raw = elm[0];

            elm.bind('scroll', function () {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                    scope.$apply(attr.whenScrolled);
                }
            });
        };
    })

    .directive('videoLoader', function(){
        return function (scope, element, attrs){
            scope.$watch("video",  function(newValue, oldValue){ //watching the scope url value
                console.log(newValue);
                console.log(element)
                element[0].attr('src',  newValue);; //set the URL on the src attribute
                element[0].load();
                element[0].play();
            }, true);

        }
    });